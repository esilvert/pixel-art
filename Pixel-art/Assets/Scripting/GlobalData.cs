using UnityEngine;

namespace Scripting
{
    public class GlobalData
    {
        /// Singleton
        private static GlobalData m_instance = null;
        internal static GlobalData instance {
            get 
            {
                if (m_instance == null)
                    m_instance = new GlobalData();
                return m_instance;
            }
            set {m_instance = value;}}

        /// The path to the image to draw
        internal string currentImageName = null;

        /// Path to Image folder
        internal string kImageFolder = Application.persistentDataPath + "/Images/"; 
    }
}