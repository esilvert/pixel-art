using System.IO;
using UnityEngine;

namespace Scripting.Utilities
{
    class FileUtils
    {
        /// Load image in persistent data folder
        internal static Texture2D LoadPersistentImage(string p_name)
        {
            Texture2D image = new Texture2D(2,2);
            Debug.LogFormat("FileUtils.LoadPersistentImage(): Trying to load image {0} from path {1}.", p_name, GlobalData.instance.kImageFolder);
            image.LoadImage( File.ReadAllBytes(GlobalData.instance.kImageFolder + p_name) );

            return image;
        }

        /// Load image from anywhere
        internal static Texture2D LoadImage(string p_full_path)
        {
            Texture2D image = new Texture2D(2,2);
            Debug.LogFormat("FileUtils.LoadPersistentImage(): Trying to load image {0}.", p_full_path);
            image.LoadImage( File.ReadAllBytes(p_full_path) );

            return image;
        }
    }
}