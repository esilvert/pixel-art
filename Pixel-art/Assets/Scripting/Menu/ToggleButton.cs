﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Scripting.Menu.UI
{
	[RequireComponent(typeof(Button))]
	class ToggleButton : MonoBehaviour 
	{
		/// The object to be toggled
		[SerializeField]
		GameObject _ToggledObject = null;

		/// Base status
		[SerializeField]
		bool _StartEnabled = true;

		/// Awake
		void Awake()
		{
			// Add callback
			GetComponent<Button>().onClick.AddListener(OnClick);

			// Base status
			_ToggledObject.gameObject.SetActive(_StartEnabled);
		}

		/// Callback - button clicked
		void OnClick()
		{
			// Toggle
			_ToggledObject.gameObject.SetActive(!_ToggledObject.gameObject.activeSelf);
		}
	}
}