﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Scripting.Utilities;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Scripting.Menu.UI
{
 	class ImageMenu : MonoBehaviour 
 	{
		 /// The prefab for the menu images
		 [SerializeField]
		 GameObject _MenuImagePrefab = null;

		/// Pen icon object
		[SerializeField]
		GameObject _PenIcon = null;

		/// The current selected image
		GameObject m_selected = null;

		/// Awake
		void Awake()
		{
			Refresh();
		}

		/// Refresh
		public void Refresh()
		{
			// Delete all childs
			for(int i = 0 ; i < transform.childCount ; ++i)
			{
				Destroy(transform.GetChild(i).gameObject);
			}

			// Ensure directory exists 
			if (Directory.Exists(GlobalData.instance.kImageFolder) == false)
			{
				Directory.CreateDirectory(GlobalData.instance.kImageFolder);
				Debug.LogFormat("ImageMenu.Awake(): Created Images directory.");
			}

			// Parse directory
			string[] image_names = Directory.GetFiles(GlobalData.instance.kImageFolder);

			foreach(string p_file_name in image_names)
			{
				// Ignore .save files
				if (Path.GetExtension(p_file_name) != ".png")
					continue;

				// Show final image OR current progress
				Texture2D texture = FileUtils.LoadImage(p_file_name);
				Texture2D currentProgress = null;

				if (File.Exists(p_file_name + ".save") == true)
				{
					currentProgress = FileUtils.LoadImage(p_file_name + ".save");
				}
				 

				// Image too large
				// if (texture.width * texture.height > GlobalData.kMaxImageSize)
				// {
				// 	Debug.LogWarningFormat("ImageMenu.Refresh(): Image {0} is too large (size is {1}, max allowed is {2}).", p_file_name, texture.width * texture.height, GlobalData.kMaxImageSize);
				// 	continue;
				// }

				GameObject child = GameObject.Instantiate(_MenuImagePrefab);
				child.transform.SetParent(transform);
				child.gameObject.name = Path.GetFileName(p_file_name);

				// Setup menu entry
				MenuEntry menuEntry = child.GetComponent<MenuEntry>();
				menuEntry.realTexture = texture;
				menuEntry.currentTexture = currentProgress;
				menuEntry.texturePath = p_file_name;
				menuEntry.deleted += OnImageDeleted;

				// Set callback
				Button button = child.GetComponent<Button>();
				button.onClick.AddListener( OnImageClicked );
			}
		}

		/// Callback - Image clicked
		 public void OnImageClicked()
		 {
			 for(int i = 0 ; i < transform.childCount; ++i)
			 {
				 Transform transf = transform.GetChild(i);
				 
				 if (EventSystem.current.currentSelectedGameObject == transf.gameObject)
				 {
					// Update current image name
					GlobalData.instance.currentImageName = transf.gameObject.name;

					// Put icon
					_PenIcon.gameObject.SetActive(true);
					m_selected = transf.gameObject;
				 }
			 }
		 }

		/// <summary>
		/// Callback - Image delete
		/// </summary>
		/// <param name="p_entry"></param>
		 public void OnImageDeleted(MenuEntry p_entry)
		 {
			 p_entry.deleted -= OnImageDeleted;
			 _PenIcon.gameObject.SetActive(false);
		 }

		/// Update
		 void Update()
		 {
			 // Follow selected image
			 if (m_selected != null)
				_PenIcon.transform.position = m_selected.transform.position;
			
		 }
	}
}
