﻿using System.IO;
using UnityEngine;

namespace Scripting.Menu.UI
{
	class MenuEntry : MonoBehaviour 
	{
		/// The true image 
		internal Texture2D realTexture = null;

		/// The current progression
		internal Texture2D currentTexture = null;

		/// The path to the real texture
		internal string texturePath = null;

		/// The erase save button
		[SerializeField]
		UnityEngine.UI.Button _ResetSave = null;

		/// Real texture display
		[SerializeField]
		UnityEngine.UI.Image _RealTexture = null;

		/// If a progress exists, put it in this texture
		[SerializeField]
		UnityEngine.UI.Image _CurrentTexture = null;

		/// <summary>
		/// Event - On delete
		/// </summary>
		internal System.Action<MenuEntry> deleted = null;

		/// Awake
		void Start()
		{
			Debug.Assert(realTexture != null, "MenuEntry.Awake(): Real texture not set.");
			Debug.Assert(texturePath != null, "MenuEntry.Awake(): Texture path not set.");

			Debug.Assert(_RealTexture != null, "MenuEntry.Awake(): Real image not bind.");
			Debug.Assert(_CurrentTexture != null, "MenuEntry.Awake(): Real image not bind.");
			Debug.Assert(_ResetSave != null, "MenuEntry.Awake(): Erase button not bind.");

			// Set real texture			
			_RealTexture.overrideSprite = Sprite.Create(realTexture, new Rect(0,0, realTexture.width, realTexture.height), Vector2.one * 0.5f);

			// Current progress
			if (currentTexture != null)
			{
				_CurrentTexture.overrideSprite =  Sprite.Create(currentTexture, new Rect(0,0, currentTexture.width, currentTexture.height), Vector2.one * 0.5f);
			}
			else
			{
				_CurrentTexture.gameObject.SetActive(false);
			}

			// Enable erase button
			_ResetSave.gameObject.SetActive(currentTexture != null);
		}

		

		/// Callback - Erasing save 
		public void OnResetSavePressed()
		{
			_ResetSave.gameObject.SetActive(false);
			File.Delete(texturePath + ".save");
			_CurrentTexture.overrideSprite = null;
			_CurrentTexture.gameObject.SetActive(false);
		}

		/// Callback - Erasing save 
		public void OnEraseFilePressed()
		{
			// Trigger event
			if (deleted != null)
				deleted(this);

			// Disable reset button
			_ResetSave.gameObject.SetActive(false);

			// Delete save file
			if(File.Exists(texturePath + ".save") == true)
				File.Delete(texturePath + ".save");
			
			// Delete image file
			File.Delete(texturePath);

			// Destroy object
			Destroy(gameObject);
		}

	}
}