﻿
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace Scripting.Menu.UI
{
 	class ImportButton : MonoBehaviour 
	{
		/// The content refresher
		[SerializeField]
		ImageMenu _ContentRefresher = null;

		// Debug text field
		[SerializeField]
		Text _DebugText = null;

		/// <summary>
		/// The preview image
		/// </summary>
		[SerializeField]
		Image _ImportPreview = null;

		/// <summary>
		/// The import UI
		/// </summary>
		[SerializeField]
		GameObject _ImportUI = null;

		/// <summary>
		/// The value of color simplicity label
		/// </summary>
		[SerializeField]
		Text _ColorSimplicityValue = null;

		/// <summary>
		/// The sliders
		/// </summary>
		[SerializeField]
		Slider _SizeSlider = null;

		[SerializeField]
		Slider _ColorDiversitySlider = null;

		/// <summary>
		/// The new image name
		/// </summary>
		string m_newImageName = "unkown.png";

		/// <summary>
		/// The image path
		/// </summary>
		string m_imagePath = "unknown.png";

		internal string normalizeName
		{
			get
			{
				return Path.GetFileNameWithoutExtension(m_newImageName) + "x" + _SizeSlider.value.ToString() + "-" + _ColorDiversitySlider.value.ToString();
			}
		}


		/// Callback - Import button clicked
		public void OnImportClicked()
		{
			#if(UNITY_EDITOR)
				OnImageFromGallerySelected("C:\\Users\\esilv\\Documents\\Home\\Pictures\\avatar\\03sP2cWvGo.png");
			#else
				NativeGallery.GetImageFromGallery(OnImageFromGallerySelected);
			#endif
			// OnImageFromGallerySelected("C:\\Users\\esilv\\Documents\\Home\\Pictures\\avatar\\io.jpeg");
			// OnImageFromGallerySelected("C:\\Users\\esilv\\Documents\\Home\\Pictures\\avatar\\duck_avatar.png");
		}

		/// Callback - Image selected
		void OnImageFromGallerySelected(string p_path)
		{
			m_imagePath = p_path;
			_ImportUI.SetActive(true);
			m_newImageName = p_path;

			Texture2D result = Resize(m_imagePath);
			_ImportPreview.sprite = Sprite.Create(result, new Rect(0, 0, result.width, result.height), Vector2.one * 0.5f);
		}

		/// Resize an image that is too large
		Texture2D Resize(string p_path)
		{
			// Load big image
			Texture2D disk = new Texture2D(2,2);
			disk.LoadImage(File.ReadAllBytes(p_path));

			// Prepare smaller version
			int step = (int)(Mathf.Max(disk.width / _SizeSlider.value + 1, disk.height / _SizeSlider.value + 1));
			Texture2D result = new Texture2D(disk.width / step, disk.height / step);

			int new_x = 0, new_y = 0;

			for(int y = 0 ; y < disk.height ; y += step )
			{
				for(int x = 0 ; x < disk.width ; x += step )
				{
					Vector3 rgb_avg = Vector3.zero;
					for(int inner_y = 0 ; inner_y < step ; ++inner_y)
					{
						for(int inner_x = 0 ; inner_x < step ; ++inner_x)
						{
							if (x + inner_x >= disk.width || y + inner_y >= disk.height)
								continue;

							Color pixel = disk.GetPixel(x + inner_x, y + inner_y);
							rgb_avg += new Vector3(pixel.r, pixel.g, pixel.b);
							// Debug.LogFormat("x {0} y {1} ix {2} iy {3} step {4}", x, y, inner_x, inner_y, step);
						}
					}

					rgb_avg *= 1f/ Mathf.Pow(step, 2f);

					// Save pixel
					result.SetPixel(new_x, new_y, new Color(rgb_avg.x, rgb_avg.y, rgb_avg.z));

					++new_x;
				}
				++new_y;
				new_x = 0;
			}

			result.Apply();
			return result;
		}


		/// <summary>
		/// Callback - color diversity 
		/// </summary>
		public void OnColorDiversitySliderChanged(Slider p_slider)
		{
			_ColorSimplicityValue.text = p_slider.value.ToString();
		}

		/// <summary>
		/// Callback - Size changed
		/// </summary>
		public void OnSizeSliderChanged(Slider p_slider)
		{
			Texture2D result = Resize(m_imagePath);
			_ImportPreview.sprite = Sprite.Create(result, new Rect(0, 0, result.width, result.height), Vector2.one * 0.5f);

		}

		/// <summary>
		/// Callback - Name changed
		/// </summary>
		public void OnImageNameChanged(Text p_text)
		{
			if (Path.GetExtension(p_text.text)  == string.Empty)
			{
				p_text.text += ".png";
			}

			m_newImageName = p_text.text;

		}

		/// <summary>
		/// Callback - final import button pressed
		/// </summary>
		public void OnImportButtonPressed()
		{
			// Copy it to persistent data folder
			try
			{
				Texture2D disk = new Texture2D(2, 2);
				disk.LoadImage(File.ReadAllBytes(m_imagePath));
				float size = disk.width * disk.height;
				float newImageSize = _SizeSlider.value;
				if (size > newImageSize * newImageSize)
				{
					_DebugText.text += "ImportButton.OnImageFromGallerySelected(): Selected image is too big ! (" + size.ToString() + ", max allowed is " + (newImageSize * newImageSize).ToString() + ".\n"; 
					// Debug.LogWarning("ImportButton.OnImageFromGallerySelected(): Selected image is too big !");

					Texture2D resized = Resize(m_imagePath);

					Debug.LogWarningFormat("\tNew dimension {0}x{1}", resized.width, resized.height);
					_DebugText.text +="\tNew dimension "+ resized.width.ToString() + "x" + resized.height.ToString() + "\n";

					File.WriteAllBytes(GlobalData.instance.kImageFolder + normalizeName + ".png", resized.EncodeToPNG());
				}
				else
				{
					_DebugText.text += "\tNew image added to collection named " + m_newImageName + ".\n"; 
					File.Copy(m_imagePath, GlobalData.instance.kImageFolder + normalizeName + ".png");

					_DebugText.text += "------------ REFRESH --------------------- \n"; 
					_ContentRefresher.Refresh();
				}
			}
			catch(IOException e)
			{
				// Editor
				Debug.LogWarningFormat("ImportButton.OnImageFromGallerySelected(): Exception : {0}", e.Message);

				// IG
				_DebugText.text += "ImportButton.OnImageFromGallerySelected(): Exception : " + e.Message + "\n";
			}
			
			_ContentRefresher.Refresh();
			_ImportUI.SetActive(false);
		}
		
	}
}