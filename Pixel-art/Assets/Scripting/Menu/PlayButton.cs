﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Scripting.Menu.UI
{

	public class PlayButton : MonoBehaviour 
	{
		/// Game scene
		[SerializeField]
		string _NextScene = null;

		/// Awake
		void Awake()
		{
			GlobalData.instance.currentImageName = null;
		}

		/// Play pressed
		public void OnPlayPressed()
		{
			SceneManager.LoadScene(_NextScene);
		}

		/// Update
		void Update()
		{
			GetComponent<Button>().interactable = GlobalData.instance.currentImageName != null;
		}
	}
}