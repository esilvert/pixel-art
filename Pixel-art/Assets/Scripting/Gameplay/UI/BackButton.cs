﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Scripting.Gameplay.UI
{

	public class BackButton : MonoBehaviour 
	{
		/// Game scene
		[SerializeField]
		string _NextScene = null;

		/// Back pressed
		public void OnBackPressed()
		{
			// GlobalData.instance.currentImageName = null; // Blocks the save
			SceneManager.LoadScene(_NextScene);
		}
	}
}