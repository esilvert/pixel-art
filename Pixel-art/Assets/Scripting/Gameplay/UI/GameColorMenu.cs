﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripting.Gameplay.UI
{

    class GameColorMenu : MonoBehaviour
    {
        /// <summary>
        /// The panel height
        /// </summary>
        public static int _PanelHeight = 225;

        /// <summary>
        /// The default container for colors UI
        /// </summary>
        [SerializeField]
        GameObject _PagePrefab = null;

        /// <summary>
        /// Where to instantiate pages ?s
        /// </summary>
        [SerializeField]
        GameObject _PageContainer = null;

        /// <summary>
        /// Max number of child per page
        /// </summary>
        [SerializeField]
        int _MaxPageChildCount = 10;

        /// <summary>
        /// The pages
        /// </summary>
        List<GameObject> m_pages = null;

        /// Colors count
        Dictionary<Color,int> m_colorCount;

        /// The UI buttons
        Dictionary<Color,ColorChoice> m_colorChoices;

        /// Current selected choice
        ColorChoice m_selected = null;

        /// Allow to terminate a color
        internal bool allowValidateColor = false;

        /// <summary>
        /// Awake
        /// </summary>
        void Awake()
        {
            m_pages = new List<GameObject>(1);
            m_colorCount = new Dictionary<Color, int>();
            m_colorChoices = new Dictionary<Color,ColorChoice>();
        }

        /// <summary>
        /// Adds a color 
        /// </summary>
        internal void AddColor(ColorChoice p_child)
        {
            int lastPageIndex = m_pages.Count - 1;

            // Add additionnal page
            if (m_pages.Count == 0 || m_pages[lastPageIndex].transform.childCount == _MaxPageChildCount)
            {
                GameObject newPage = GameObject.Instantiate(_PagePrefab);
                newPage.transform.SetParent(_PageContainer.transform);
                newPage.transform.localPosition = new Vector3(Screen.width * m_pages.Count, 0);
                newPage.transform.localScale = Vector3.one;

                m_pages.Add(newPage);
                lastPageIndex++;
            }

            // Add color to page
            p_child.transform.SetParent(m_pages[lastPageIndex].transform);
            p_child.transform.localScale = Vector3.one;
            p_child.onPressed += OnColorChoicePressed;

            // Bind ui button to color
            m_colorChoices.Add(p_child.color, p_child);
            
        }

        /// Remember the count of all colors
        internal void Count(Color p_color)
        {
            if (m_colorCount.ContainsKey(p_color) == true)
            {
                m_colorCount[p_color]++;
            }
            else
            {
                m_colorCount.Add(p_color, 1);
            }
        }

        /// Add one to the counter
        internal void ValidateOnePixel(Color p_color)
        {
            ColorChoice cc = m_colorChoices[p_color];
            int total = m_colorCount[p_color];
            // Suppose color is OK
            cc.ValidateOnePixel(total, allowValidateColor);

            /// Is the game OVER ?
            bool isFinished = true;
            foreach(ColorChoice ccc in m_colorChoices.Values)
            {
                if (ccc.count != m_colorCount[ccc.color])
                {
                    isFinished = false;
                    break;
                }
            }

            if (isFinished == true)
            {
                Debug.LogFormat("GAME OVER !!");
            }
        }

        /// <summary>
        /// Finalize menu
        /// </summary>
        internal void finalizeMenu()
        {
            Swipable swipe = _PageContainer.GetComponent<Swipable>();
            swipe._MaxStepX = new Vector2(0, m_pages.Count - 1);

            foreach(ColorChoice cc in m_colorChoices.Values)
            {
                cc.UpdateCount(m_colorCount[cc.color]);
                cc.CheckIfFinished(m_colorCount[cc.color]);
            }
        }

        /// Callback - Color choice pressed
        void OnColorChoicePressed(ColorChoice p_newSelected)
        {
            // Clicked again on the same color choice
            if (m_selected == p_newSelected)
                return;

            // Not first time we click on a color choice
            if (m_selected != null)
                m_selected.StartCoroutine("Scale", false);

            // Remember which one is selected
            m_selected = p_newSelected;

            // Play growing anim
            m_selected.StartCoroutine("Scale", true);
        }
    }
}