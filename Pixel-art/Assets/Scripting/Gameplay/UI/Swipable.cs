﻿/**
*	Created 01-27-2018
* 	Author : Vegevan
**/


using UnityEngine;

namespace Scripting.Gameplay.UI
{

    class Swipable : MonoBehaviour
    {
        /// <summary>
        /// The swipable object
        /// </summary>
        public RectTransform _Swipable = null;

        /// <summary>
        /// Sensibility
        /// </summary>
        public Vector2 _Sensitivity = Vector2.one;

        /// <summary>
        /// Steps
        /// </summary>
        public Vector2 _Step = Vector2.one * 32;

        /// <summary>
        /// Max step number
        /// </summary>
        public Vector2 _MaxStepX = Vector2.zero;
        public Vector2 _MaxStepY = Vector2.zero;

        /// <summary>
        /// Time to reposition
        /// </summary>
        public float _RepositioningDelay = 0.3f;

        /// <summary>
        /// Trigger ratio   
        /// </summary>
        public float _TriggerRatio = 0.5f;

        /// <summary>
        /// Touch area 
        /// </summary>
        public Vector2 _TouchAreaY = Vector2.zero;

        /// <summary>
        /// Use to move smoothly
        /// </summary>
        Vector2 m_remaining;
        float m_releaseTimestamp;
        Vector3 m_startPosition;
        Vector3 m_originPosition;
        Vector3 m_releasePosition;

        bool m_repositionning;

        /// <summary>
        /// Start
        /// </summary>
        void Start()
        {
            Debug.Assert(_Swipable != null);

            // Init
            m_releaseTimestamp = float.NegativeInfinity;
            m_releasePosition = _Swipable.anchoredPosition;
            m_remaining = Vector2.zero;
            m_repositionning = false;

            // 
            m_originPosition = transform.position;
        }

        /// <summary>
        /// Update
        /// </summary>
        void Update()
        {
            Vector2 swipePosition = _Swipable.anchoredPosition;

            if (m_repositionning == true)
            {
                _Swipable.anchoredPosition = Vector3.Lerp(
                        m_releasePosition,
                        m_releasePosition + (Vector3)m_remaining,
                        Mathf.Clamp01((Time.unscaledTime - m_releaseTimestamp) / _RepositioningDelay));

                m_repositionning = Mathf.Clamp01((Time.unscaledTime - m_releaseTimestamp) / _RepositioningDelay) != 1;
            }
            else if (Input.touchCount == 1)
            {
                // Started touching
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    m_startPosition = swipePosition;

                }
                // Moving
                else if (Input.GetTouch(0).phase == TouchPhase.Moved)
                {
                    if ( Input.GetTouch(0).position.y < _TouchAreaY.y && Input.GetTouch(0).position.y >= _TouchAreaY.x)
                    {
                        Vector2 delta = Input.GetTouch(0).deltaPosition;
                        _Swipable.Translate(new Vector3(_Sensitivity.x * delta.x, _Sensitivity.y * delta.y));
                        // Debug.LogFormat("Delta = {0}", delta);
                    }
                }
                // Released
                else if (Input.GetTouch(0).phase == TouchPhase.Ended)
                {
                    float stepX = _Step.x * Screen.width;
                    float stepY = _Step.y * Screen.height;

                    m_remaining.x = -(swipePosition.x - m_startPosition.x);
                    if (Mathf.Abs(m_remaining.x) > stepX * _TriggerRatio)
                        m_remaining.x -= Mathf.Sign(m_remaining.x) * stepX;

                    int totalShiftX = (int)((m_originPosition.x - swipePosition.x - m_remaining.x) / stepX);
                    if (totalShiftX > (int)_MaxStepX.y || totalShiftX < (int)_MaxStepX.x)
                        m_remaining.x -= Mathf.Sign(m_remaining.x) * stepX;

                    m_remaining.y = -(swipePosition.y - m_startPosition.y);
                    if (Mathf.Abs(m_remaining.y) > stepY * _TriggerRatio)
                        m_remaining.y -= Mathf.Sign(m_remaining.y) * stepY;

                    int totalShiftY = (int)((m_originPosition.y - swipePosition.y - m_remaining.y) / stepY);
                    if (totalShiftY > (int)_MaxStepY.y || totalShiftY < (int)_MaxStepY.x)
                        m_remaining.y -= Mathf.Sign(m_remaining.y) * stepY;



                    m_releaseTimestamp = Time.unscaledTime;
                    m_releasePosition = swipePosition;
                    m_repositionning = true;
                }
            }
        }
    }
}