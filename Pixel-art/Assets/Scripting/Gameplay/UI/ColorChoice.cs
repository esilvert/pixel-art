﻿using Scripting.Gameplay.GameManagers;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;// Required when using Event data.
using System.Collections;

namespace Scripting.Gameplay.UI
{

    class ColorChoice : MonoBehaviour, IPointerDownHandler
    {
        /// <summary>
        /// The selectable color
        /// </summary>
        [SerializeField]
        Color _Color = Color.white;
        internal Color color { get { return _Color; } }

        /// <summary>
        /// The text
        /// </summary>
        [SerializeField]
        Text _Text = null;

        /// <summary>
        /// The count
        /// </summary>
        [SerializeField]
        Text _Count = null;

        /// The validated image
        [SerializeField]
        GameObject _Validated = null;


        /// Growing animation duration
        [SerializeField]
        float _GrowingAnimationDuration = 0.3f;

        /// Default scale
        [SerializeField]
        float _DefaultScale = 0.8f;

        /// <summary>
        /// The image
        /// </summary>
        UnityEngine.UI.Image m_image;

        /// The current count
        int m_currentCount = 0;
        internal int count {get {return m_currentCount;}}


        /// Callback - On Pressed
        internal System.Action<ColorChoice> onPressed;

        /// True once finished
        bool m_isFinished = false;

        float m_scaleDuration = 0f;


        /// <summary>
        /// Awake
        /// </summary>
        void Awake()
        {
            // Get the image
            m_image = GetComponent<UnityEngine.UI.Image>();
            
            // Disable validated
            _Validated.gameObject.SetActive(false);
        }

        /// Start
        void Start()
        {
            transform.localScale = Vector3.one * _DefaultScale;
        }

        /// <summary>
        /// Callback - Pointer Down
        /// </summary>
        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
            if (m_isFinished == false)
            {
                Managers.instance.colorManager.currentColor = _Color;

                if(onPressed != null)
                    onPressed(this);
            }
        }

        /// <summary>
        /// Change ColorChoice's color
        /// </summary>
        internal void SetColor(Color p_color, int p_index)
        {
            // Color
            _Color = p_color;
            m_image.color = p_color;

            // Text
            _Text.text = p_index.ToString();
        }

        /// Update the count
        internal void ValidateOnePixel(float p_total, bool p_allowValidateColor = true)
        {
            ++m_currentCount;
            UpdateCount(p_total);

            if (p_allowValidateColor == true)
                CheckIfFinished(p_total);
        }

        /// Validate a color choice
        internal void CheckIfFinished(float p_total)
        {
            if (m_currentCount == p_total)
            {
                m_isFinished = true;
                _Text.gameObject.SetActive(false);
                _Count.gameObject.SetActive(false);
                _Validated.gameObject.SetActive(true);
            }
        }

        /// Update the text label
        internal void UpdateCount(float p_total)
        {
            _Count.text = m_currentCount.ToString() + "/" + p_total.ToString();
        }

        /// Growing animation
        internal IEnumerator Scale(bool p_trueToExpand)
        {
            m_scaleDuration = 0f;

            while(m_scaleDuration < _GrowingAnimationDuration)
            {
                m_scaleDuration += Time.deltaTime;
                float min = p_trueToExpand == true ? _DefaultScale : 1f;
                float max = p_trueToExpand == true ? 1f : _DefaultScale;
                transform.localScale = Vector3.one * Mathf.Lerp(min, max, m_scaleDuration / _GrowingAnimationDuration);
                yield return new WaitForEndOfFrame(); 
            }


        }
    }
}