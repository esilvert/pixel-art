﻿using UnityEngine;

namespace Scripting.Gameplay.GameManagers
{

    class ColorManager : MonoBehaviour
    {
        /// <summary>
        /// The current Color
        /// </summary>
        internal Color currentColor { get; set; }
        
        /// Returns the opposite color
        internal static Color GetOppositeColor(Color p_color, float p_alpha = 1f)
        {
            return new Color( Mathf.Sqrt( Mathf.Pow(1f - p_color.r, 2f)), Mathf.Sqrt( Mathf.Pow(1f - p_color.g, 2f)), Mathf.Sqrt( Mathf.Pow(1f - p_color.b, 2f)), p_alpha);
        }

        /// Returns the opposite grey
        internal static Color GetOppositeGrey(float p_grey, float p_alpha = 1f)
        {
            if ( p_grey < 0.4 || p_grey > 0.6f)
            {
                float opGrey = Mathf.Sqrt( Mathf.Pow(1f - p_grey, 2f));
                return new Color( opGrey, opGrey, opGrey, p_alpha);
            }
            else
            {
                return new Color(0, 0, 0, p_alpha);
            }
        }
    }
}