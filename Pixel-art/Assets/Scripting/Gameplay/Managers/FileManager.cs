﻿using UnityEngine;

namespace Scripting.Gameplay.GameManagers
{

    class FileManager : MonoBehaviour
    {
        /// <summary>
        /// Wrapper
        /// </summary>
        /// <param name="p_callback">Function taking string param which is the file path</param>
        /// <returns></returns>
        internal NativeGallery.Permission GetImageFromGallery(NativeGallery.MediaPickCallback p_callback, string p_title = "", string p_mime = "image/*", int p_maxSize = -1)
        {
            return NativeGallery.GetImageFromGallery(p_callback, p_title, p_mime, p_maxSize);
        }
    }
}