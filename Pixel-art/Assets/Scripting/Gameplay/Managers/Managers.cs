﻿using UnityEngine;

namespace Scripting.Gameplay.GameManagers
{
    class Managers : MonoBehaviour
    {
        /// <summary>
        /// The singleton instance
        /// </summary>
        static internal Managers instance { get; private set;}

        /// <summary>
        /// Color manager
        /// </summary>
        internal ColorManager colorManager { get; private set; }

        /// <summary>
        /// The file manager
        /// </summary>
        internal FileManager fileManager { get; private set; }

        void Awake()
        {
            // Singleton
            if (instance != null)
            {
                // Destroy the manager object
                Destroy(gameObject);
            }
            else
            {
                instance = this;

                // Get color manager
                colorManager = GetComponent<ColorManager>();

                // Get the file manager
                fileManager = GetComponent<FileManager>();
            }
        }

    }
}