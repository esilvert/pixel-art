﻿using Scripting.Gameplay.GameManagers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Scripting.Gameplay
{

    class Coloriable : MonoBehaviour
    {
        /// <summary>
        /// The clue text
        /// </summary>
        [SerializeField]
        TextMesh _Text = null;


        /// <summary>
        /// The sprite
        /// </summary>
        SpriteRenderer m_renderer;
        internal new SpriteRenderer renderer { get { return m_renderer; } }

        /// <summary>
        /// The right color from the image
        /// </summary>
        internal Color m_realColor;
        internal Color initialColor;
        Color m_currentColor;
        Color m_overrideColor;

        /// <summary>
        /// True to disable
        /// </summary>
        bool m_disabled = false;
        internal bool disabled {get {return m_disabled;}}

        void Awake()
        {
            // Get sprite renderer
            m_renderer = GetComponent<SpriteRenderer>();
        }

        /// <summary>
        /// Callback - touched
        /// </summary>
        internal bool OnTouch()
        {
            // Change color
            if (m_disabled == false)
            {
                Color currentColor = Managers.instance.colorManager.currentColor;
                if (currentColor == m_realColor)
                {
                    _Text.gameObject.SetActive(false);
                    m_disabled = true;
                    m_currentColor = new Color(currentColor.r, currentColor.g, currentColor.b, 1f);
                    m_renderer.color = m_currentColor;
                    return true;
                }
                else if (m_overrideColor != Color.black)
                {
                    _Text.color = ColorManager.GetOppositeColor(currentColor);
                    m_currentColor = new Color(currentColor.r, currentColor.g, currentColor.b, 0.5f);
                    m_renderer.color = m_currentColor;
                }
            }

            return false;
        }

        /// <summary>
        /// Gives a color to the cell
        /// </summary>
        internal void SetColor(Color p_color, int p_index)
        {
            // Save real color
            m_realColor = p_color;

            // Find grayscale to placeholder
            float grey = (0.2126f * p_color.r + 0.7152f * p_color.g + 0.0722f * p_color.b);
            renderer.color = new Color(grey, grey, grey, 0.8f);
            initialColor = renderer.color;
            m_currentColor = initialColor;

            // Disable transparent cells
            if (p_color.a < 1)
                m_disabled = true;

            // Set text
            _Text.text = p_index.ToString();
            _Text.color = ColorManager.GetOppositeGrey(grey);
        }

        /// <summary>
        /// Callback - Asked for help
        /// </summary>
        /// <param name="p_isHelpToggled"></param>
        internal void OnHelpToggled(bool p_isHelpToggled)
        {
            bool mustStayUnchanged = p_isHelpToggled == false || m_realColor == Managers.instance.colorManager.currentColor;

            _Text.gameObject.SetActive(m_disabled == false && mustStayUnchanged);
            m_overrideColor = mustStayUnchanged == true ? m_currentColor : Color.black;
            m_renderer.color = m_overrideColor;
        }
    }
}