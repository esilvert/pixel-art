﻿using Scripting.Gameplay.GameManagers;
using Scripting.Gameplay.UI;
using Scripting.Utilities;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace Scripting.Gameplay
{

    class Image : MonoBehaviour
    {
        [SerializeField]
        GameObject _ColoriablePrefab = null;

        /// <summary>
        /// The UI prefab
        /// </summary>
        [SerializeField]
        GameObject _ColorChoicePrefab = null;

        /// <summary>
        /// The size of one coloriable
        /// </summary>
        [SerializeField]
        float _ColoriableSize = 0.2f;
        internal float coloriableSize {get {return _ColoriableSize;} }

        /// <summary>
        /// The grid
        /// </summary>
        [SerializeField]
        GameColorMenu _Menu = null;
        internal GameColorMenu menu {get {return _Menu;}}

        /// <summary>
        /// Delta E tolerance
        /// </summary>
        [SerializeField]
        float _DeltaETolerance = 20f;

        /// <summary>
        /// The source image
        /// </summary>
        Texture2D m_image = null;
        Texture2D m_imageSave = null;

        /// <summary>
        /// The colors
        /// </summary>
        List<Color> m_colors;

        /// <summary>
        /// True to enable help
        /// </summary>
        bool m_isHelpedToggled = false;

        /// <summary>
        /// Start
        /// </summary>
        void Start()
        {
            m_image = new Texture2D(2,2);

            // EDITOR ONLY
            if (GlobalData.instance.currentImageName == null)
                GlobalData.instance.currentImageName = "unicorn.png";
            //

            /// Load the image
            m_image = Resources.Load(GlobalData.instance.currentImageName) as Texture2D ;
            m_image = FileUtils.LoadPersistentImage(GlobalData.instance.currentImageName);
            if (File.Exists(GlobalData.instance.kImageFolder + GlobalData.instance.currentImageName + ".save") == true)
            {
                m_imageSave = FileUtils.LoadPersistentImage(GlobalData.instance.currentImageName + ".save");
            }
            // m_image = sp.texture; // TODO HERE

            // Delta E (optionnal for retro comptability)
            int indexStart = GlobalData.instance.currentImageName.LastIndexOf('-') + 1;
            int indexEnd = GlobalData.instance.currentImageName.LastIndexOf('.');
            if (indexStart - 1 != -1)
                _DeltaETolerance =  float.Parse(GlobalData.instance.currentImageName.Substring(indexStart, indexEnd - indexStart));

            Debug.LogFormat("Name {0} deltaE {1}", GlobalData.instance.currentImageName, _DeltaETolerance);


            Debug.AssertFormat(m_image != null, "Image.Start(): Given image is null.");
            Debug.AssertFormat(_ColoriablePrefab != null, "Image.Start(): No prefab to instantiate.");

            m_colors = new List<Color>();

            _Menu.allowValidateColor = false;

            // Open image
            //Managers.instance.fileManager.GetImageFromGallery(OnMediaPicked); // To load custum picture

            // Create everything from image
            for (int y = 0; y < m_image.height; ++y)
            {
                for (int x = 0; x < m_image.width ; ++x)
                {
                    // Get pixel
                    Color pixel = m_image.GetPixel(x, y);
                    if (pixel.a > 0.2f)
                        pixel.a = 1;

                    // Skip transparent pixels
                    //if (pixel.a == 0)
                    //    continue;

                    /// Find similar colors
                    Color finalColor = pixel;
                    int clueValue = -1;
                    bool isNewColor = true;
                    for(int i = 0; i < m_colors.Count; ++i)
                    {
                        Color c = m_colors[i];

                        LABColor pixelLAB = new LABColor(pixel);
                        LABColor currentLAB = new LABColor(c);

                        float deltaE = Mathf.Sqrt(
                            Mathf.Pow((pixelLAB.l - currentLAB.l),2) +
                            Mathf.Pow((pixelLAB.a - currentLAB.a),2) +
                            Mathf.Pow((pixelLAB.b - currentLAB.b),2)
                            );

                        // http://zschuessler.github.io/DeltaE/learn/#toc-delta-e-2000
                        if ( deltaE < _DeltaETolerance)
                        {
                            isNewColor = false;
                            finalColor = c;
                            clueValue = i;
                            break;
                        }
                    }

                    // UI
                    if (isNewColor == true)
                    {
                        m_colors.Add(pixel);
                        clueValue = m_colors.Count - 1;

                        GameObject go = GameObject.Instantiate(_ColorChoicePrefab);
                        ColorChoice cc = go.GetComponent<ColorChoice>();
                        cc.SetColor(pixel, m_colors.Count - 1);
                        _Menu.AddColor(cc);

                        // Special case - First color
                        if (m_colors.Count == 1)
                        {
                            Managers.instance.colorManager.currentColor = pixel;
                        }
                    }

                    _Menu.Count(finalColor);

                    // Cell 
                    GameObject obj = GameObject.Instantiate(_ColoriablePrefab);

                    // Set parent
                    obj.transform.SetParent(transform);

                    // Set position
                    obj.transform.position = new Vector2(x, y) * _ColoriableSize;

                    // Get coloriable component
                    Coloriable coloriable = obj.GetComponent<Coloriable>();
                    coloriable.SetColor(finalColor, clueValue); // Give real color

                    if (m_imageSave != null)
                    {
                        Color savedPixel = m_imageSave.GetPixel(x,y);
                        Managers.instance.colorManager.currentColor = savedPixel;

                        if (savedPixel.a != 0f && coloriable.OnTouch() == true)
                        {   
                            _Menu.ValidateOnePixel(coloriable.m_realColor);
                        }
                    }
                }
            }

            // Finalize menu
            _Menu.allowValidateColor = true;
            _Menu.finalizeMenu();
        }

        /// <summary>
        /// Returns coloriable a (x,y) pos
        /// </summary>
        internal Coloriable GetColoriable(int p_x, int p_y)
        {
            int index = p_y * m_image.width + p_x;

            // Security
            if (index < 0 || index >= transform.childCount)
                return null;

            return transform.GetChild(index).GetComponent<Coloriable>();
        }

        /// Save image
        public void SaveImage()
        {
            // Create current coloration
            Texture2D save = new Texture2D(m_image.width, m_image.height);
            int x = 0, y = 0;
            for(int i = 0 ; i < transform.childCount ; ++i)
            {
                Coloriable c = transform.GetChild(i).GetComponent<Coloriable>();
                Color savedColor = c.renderer.color;
                if (c.initialColor != c.renderer.color)
                    save.SetPixel(x,y, savedColor);
                else
                    save.SetPixel(x, y, Color.clear);
                   

                // Keep coordinates
                x++;
                if (x == m_image.width)
                {
                    x =  0;
                    y++;
                }   
            }

            // Actually apply
            save.Apply();

            // Save
            FileStream f = File.OpenWrite(GlobalData.instance.kImageFolder + GlobalData.instance.currentImageName + ".save");
            byte[] bytes = save.EncodeToPNG();
            foreach(byte b in bytes)
                f.WriteByte(b);
            
            // Debug
            Debug.Log("ImageImage(): Successfully saved the current image coloration.");

            f.Close();
        }

        /// Callback - Paused (Quit isn't necessary called on Android)
        void OnApplicationPause()
        {
            Debug.LogWarning("Image.OnApplicationPause(): Emergency save launched !");
            SaveImage();
        }

        /// <summary>
        /// Callback sent to all game objects before the application is quit.
        /// </summary>
        void OnApplicationQuit()
        {
            Debug.LogWarning("Image.OnApplicationQuit(): Emergency save launched !");
            SaveImage();
        }

        /// <summary>
        /// Callback - Help pressed
        /// </summary>
        public void OnHelpToggled()
        {
            m_isHelpedToggled = !m_isHelpedToggled;
            Debug.LogFormat("Image.OnHelpToggled(): Help toggled, value = {0}.", m_isHelpedToggled);

            for(int i = 0 ; i < transform.childCount ; ++i)
            {
                SpriteRenderer spriteRenderer = transform.GetChild(i).GetComponent<SpriteRenderer>();
                Coloriable coloriable = transform.GetChild(i).GetComponent<Coloriable>();

                if (spriteRenderer != null && coloriable != null)
                {
                   coloriable.OnHelpToggled(m_isHelpedToggled);
                }
            }
        }
    }

}