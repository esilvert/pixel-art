﻿using Scripting.Gameplay;
using Scripting.Gameplay.UI;
using UnityEngine;

namespace Scripting.Gameplay.Handler
{


    class CameraHandler : MonoBehaviour
    {
        enum EMode 
        {
            None,
            Coloration,
            Moving
        }

        /// <summary>
        /// The image
        /// </summary>
        [SerializeField]
        Image _Image = null;

        /// <summary>
        /// Zoom parameters
        /// </summary>
        [Header("Zoom Parameters")]
        [SerializeField]
        float _MinZoom = 0.5f;

        [SerializeField]
        float _MaxZoom = 5f;

        // [SerializeField]
        // float _DoubleFingerSpacingThreshold = 20f;

        /// <summary>
        /// The zoom speed
        /// </summary>
        [SerializeField]
        float _ZoomSpeed = 0.2f;

        /// Minimum duration to wait to pass in coloration mode
        [SerializeField]
        float _StationnaryDurationThreshold = 0.2f;

        /// <summary>
        /// The minimum delta position to have before considering a moving state
        /// </summary>
        [SerializeField]
        float _MinimumMovementMagnitudeThreshold = 2.2f;

        /// <summary>
        /// The camera
        /// </summary>
        Camera m_camera = null;

        /// <summary>
        /// Distance between fingers
        /// </summary>
        float m_zoomInitialFingerDistance = float.MaxValue;
        // float m_zoomInitialSize = 1f;

        /// Last distance bbetween fingers
        float m_lastFingerDistance = -1;

        /// Current mode
        EMode m_mode = EMode.None;

        /// Time spent stationnary
        float m_stationnaryTime = 0f;

        #if(UNITY_EDITOR)
        Vector2 m_lastMousePosition;
        #endif

        /// <summary>
        /// Start
        /// </summary>
        void Start()
        {
            // Get the camera
            m_camera = GetComponent<Camera>();

            Debug.AssertFormat(_Image != null, "CameraHandler.Start(): Cannot interact with null image.");
        }

        /// <summary>
        ///  Update
        /// </summary>
        // public UnityEngine.UI.Text temp = null;
        void Update()
        {
            int touchCount = Input.touchCount;
        #if(UNITY_EDITOR)
            touchCount = 1;
        #endif
            switch (touchCount)
            {
                default: break;

                case 1:
                    {
                        Touch touch;
                    #if(UNITY_EDITOR)
                        // Setup touch *as if*
                        touch = new Touch();
                        touch.phase = TouchPhase.Canceled;
                        touch.position = Input.mousePosition;

                        // Setup right phase with mouse
                        if (Input.GetMouseButtonDown(0) == true)
                            touch.phase = TouchPhase.Began;
                        if (Input.GetMouseButtonUp(0) == true)
                            touch.phase = TouchPhase.Ended;

                        if (Input.GetMouseButton(0) == true && touch.phase == TouchPhase.Canceled)
                        {
                            if (touch.position == m_lastMousePosition)
                                touch.phase = TouchPhase.Stationary;
                            else
                                touch.phase = TouchPhase.Moved;
                        }         


                        touch.deltaPosition = touch.position - m_lastMousePosition;

                        if (touch.phase == TouchPhase.Canceled)
                            break;
                    #else
                        touch = Input.GetTouch(0);
                    #endif
                        Debug.LogFormat("Current touch phase : {0}", touch.phase);

                        if (touch.position.y < GameColorMenu._PanelHeight + 10)
                            break;

                        // Switching mode
                        if (touch.phase == TouchPhase.Stationary && m_mode != EMode.Moving)
                        {
                            m_stationnaryTime += Time.deltaTime;

                            if (m_stationnaryTime > _StationnaryDurationThreshold)
                            {
                                Debug.Log("CameraHandler.Update(): Switched to Coloration mode.");
                                m_mode = EMode.Coloration;
                            }
                        }
                        else if (m_mode != EMode.Coloration && touch.phase == TouchPhase.Moved && touch.deltaPosition.magnitude > _MinimumMovementMagnitudeThreshold)
                        {
                            Debug.LogFormat("CameraHandler.Update(): Switched to Moving mode (Dxy = {0}).", touch.deltaPosition);
                            m_mode = EMode.Moving;
                        }
                        else if (m_mode == EMode.None && touch.phase == TouchPhase.Ended)
                        {
                            Debug.Log("CameraHandler.Update(): Switched to Coloration mode.");
                            m_mode = EMode.Coloration;
                        }

                        // temp.text = touch.phase.ToString() + " " + m_stationnaryTime.ToString() + "/" + _StationnaryDurationThreshold.ToString();

                        // Coloration
                        if (m_mode == EMode.Coloration)
                        {
                            /// Get concerned coloriable
                            // Get largest radius
                            float radius = 0.1f;// touch.radius + touch.radiusVariance;

                            // Get world position
                            Vector2 worldPosition = m_camera.ScreenToWorldPoint(touch.position);

                            // Debug
                            Debug.LogFormat("Touch detected at {0} matching world position {1}.", touch.position, worldPosition);
                            Debug.DrawLine(worldPosition + Vector2.up * radius, worldPosition + Vector2.down * radius, Color.red);
                            Debug.DrawLine(worldPosition + Vector2.left * radius, worldPosition + Vector2.right * radius, Color.red);

                            int x = (int)(worldPosition.x / _Image.coloriableSize + 0.5f);
                            int y = (int)(worldPosition.y / _Image.coloriableSize + 0.5f);

                            Coloriable colo = _Image.GetColoriable(x, y);
                            if (colo != null && (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Moved))
                            {
                                bool isTrueColor = colo.OnTouch();

                                if (isTrueColor == true)
                                {
                                    _Image.menu.ValidateOnePixel(colo.m_realColor);
                                }
                            }
                        }

                        // Moving
                        if (m_mode == EMode.Moving)
                        { 
                            Vector2 delta = m_camera.ScreenToViewportPoint(touch.deltaPosition);
                            m_camera.transform.Translate(-delta * m_camera.orthographicSize);
                        }

                        // Release
                        if (touch.phase == TouchPhase.Ended)
                        {
                            m_mode = EMode.None;
                            m_stationnaryTime = 0f;
                        }
                    }
                    break;

                case 2:
                    {
                        Touch f1 = Input.GetTouch(0);
                        Touch f2 = Input.GetTouch(1);

                        if (f1.phase == TouchPhase.Began || f2.phase == TouchPhase.Began)
                        {
                            m_zoomInitialFingerDistance = Vector2.Distance(f1.position, f2.position);
                            // m_zoomInitialSize = m_camera.orthographicSize;
                            m_lastFingerDistance = m_zoomInitialFingerDistance;
                        }
                        else if (f1.phase == TouchPhase.Moved || f2.phase == TouchPhase.Moved )
                        {
                            float distance = Vector2.Distance(f1.position, f2.position);
                            // float newZoom = Mathf.Clamp( distance / (m_zoomInitialFingerDistance + _DoubleFingerSpacingThreshold) * m_zoomInitialSize, _MinZoom, _MaxZoom); //(_MinZoom + m_camera.orthographicSize) / _MaxZoom
                            Vector2 screenDelta = (f1.deltaPosition + f2.deltaPosition) * -0.5f;
                            screenDelta = new Vector2(screenDelta.x / Screen.currentResolution.width, screenDelta.y / Screen.currentResolution.height);
                            if (distance < m_zoomInitialFingerDistance)
                                m_camera.orthographicSize = Mathf.Clamp(m_camera.orthographicSize + (m_lastFingerDistance - distance) * _ZoomSpeed, _MinZoom, _MaxZoom);
                            else 
                                m_camera.orthographicSize = Mathf.Clamp(m_camera.orthographicSize + (m_lastFingerDistance - distance) * _ZoomSpeed, _MinZoom, _MaxZoom);
                            
                            m_lastFingerDistance = distance;
                        }
                    }
                    break;
            }

            #if(UNITY_EDITOR)
                m_lastMousePosition = Input.mousePosition;
            #endif
        }
    }
}
